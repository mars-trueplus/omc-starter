<?php
/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magestore\StockManagementSuccess\Block\Adminhtml\Warehouse\Edit\Buttons;

/**
 * Class Back
 */
class Hide implements \Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Back'),
            'class' => 'back',
            'sort_order' => 10,
            'style' => 'display:none',
            'visible' => false,
        ];
    }
}
