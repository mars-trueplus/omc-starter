<?php
/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magestore\StockManagementSuccess\Controller\Adminhtml\ManageStock;


class Index extends \Magestore\InventorySuccess\Controller\Adminhtml\AbstractAction
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magestore_InventorySuccess::warehouse_stock_view';

    /**
     * @var \Magestore\InventorySuccess\Controller\Adminhtml\Context
     */
    protected $_context;
    
    /**
     * @var \Magestore\InventorySuccess\Api\Warehouse\WarehouseManagementInterface 
     */
    protected $warehouseManagement;
    

    public function __construct(
        \Magestore\InventorySuccess\Controller\Adminhtml\Context $context,
        \Magestore\InventorySuccess\Api\Warehouse\WarehouseManagementInterface $warehouseManagement 
    ){
        parent::__construct($context);
        $this->_context = $context;
        $this->warehouseManagement = $warehouseManagement;
    }

    /**
     * Manage Stock page redirection
     *
     * @return \Magento\Backend\Model\View\Result\Page
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $primaryWarehouseId = $this->warehouseManagement->getPrimaryWarehouse()->getId();
        $resultRedirect = $this->resultRedirectFactory->create();
        if($primaryWarehouseId) {
            return $resultRedirect->setPath('inventorysuccess/manageStock/index', ['warehouse_id' => $primaryWarehouseId]);
        } else {
            return $resultRedirect->setPath('inventorysuccess/manageStock/index');
        }
    }
}