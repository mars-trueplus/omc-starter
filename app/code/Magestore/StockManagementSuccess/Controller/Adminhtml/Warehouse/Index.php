<?php
/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magestore\StockManagementSuccess\Controller\Adminhtml\Warehouse;

/**
 * Class Index
 * @package Magestore\StockManagementSuccess\Controller\Adminhtml\Warehouse
 */
class Index extends \Magestore\InventorySuccess\Controller\Adminhtml\Warehouse\Index
{
    /**
     * Warehouse grid
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $warehouseManagement = $this->_objectManager->create('\Magestore\InventorySuccess\Api\Warehouse\WarehouseManagementInterface');
        $primaryWarehouseId = $warehouseManagement->getPrimaryWarehouse()->getId();
        $resultRedirect = $this->resultRedirectFactory->create();
        if($primaryWarehouseId) {
            return $resultRedirect->setPath('inventorysuccess/warehouse/edit', ['id' => $primaryWarehouseId]);
        } else {
            return $resultRedirect->setPath('inventorysuccess/warehouse/index');
        }
    }
}