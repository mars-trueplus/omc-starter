<?php
/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magestore\StockManagementSuccess\Model\Options\Location;

class Warehouse extends \Magestore\InventorySuccess\Model\Options\Location\Warehouse
implements \Magento\Framework\Data\OptionSourceInterface
{

    /**
     * Return array of options as value-label pairs.
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray($locationId = null)
    {
        if($locationId){
            $this->_mapCollection->addFieldToFilter('location_id',['neq'=>$locationId]);
        }
        $warehouseMapIDs = $this->_mapCollection->getAllWarehouseIds();
        if ($warehouseMapIDs) {
            $this->_wareHouseCollection->addFieldToFilter('warehouse_id', ['nin' => $warehouseMapIDs]);
        }
        $options = [
            ['value' => 0, 'label' => __("Don't link to any Warehouses")]
        ];
        if(is_array($this->_wareHouseCollection->toOptionArray())) {
            $options = array_merge($options, $this->_wareHouseCollection->toOptionArray());
        }
        return $options;
    }

    /**
     * Return array of options as value-label pairs.
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function getAllOptionArray()
    {
        $options = [];
        if(is_array($this->_wareHouseCollection->toOptionArray())) {
            $options = array_merge($options, $this->_wareHouseCollection->toOptionArray());
        }
        return $options;
    }
}
